﻿namespace Base2art.Serialization.ItemSerialization
{
    public class BinaryItemSerializer<T> : ItemSerializerBase<T>
    {
        public BinaryItemSerializer()
            : base(new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter())
        {
        }
    }
}