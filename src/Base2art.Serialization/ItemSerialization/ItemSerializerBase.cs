﻿namespace Base2art.Serialization.ItemSerialization
{
    using System.IO;
    using System.Runtime.Serialization;

    public class ItemSerializerBase<T>
    {
        private readonly IFormatter formatter;

        public ItemSerializerBase(IFormatter formatter)
        {
            this.formatter = formatter;
        }

        public T Deserialize(byte[] data)
        {
            if (data == null)
            {
                throw new System.ArgumentNullException("data");
            }
            
            if (data.Length == 0)
            {
                throw new System.ArgumentException("data.Length must be more than '0'");
            }

            using (MemoryStream memoryStream = new MemoryStream(data))
            {
                return this.DoDeserialize(memoryStream);
            }
        }

        public byte[] Serialize(T objectGraph)
        {
            if (objectGraph == null)
            {
                throw new System.ArgumentNullException("objectGraph");
            }
            
            using (MemoryStream stream = new MemoryStream())
            {
                this.DoSerialize(objectGraph, stream);
                return stream.ToArray();
            }
        }

        public T Clone(T objectGraph)
        {
            using (var ms = new MemoryStream())
            {
                this.DoSerialize(objectGraph, ms);
                ms.Seek(0L, SeekOrigin.Begin);
                return this.DoDeserialize(ms);
            }
        }

        private T DoDeserialize(MemoryStream memoryStream)
        {
            return (T)this.formatter.Deserialize(memoryStream);
        }

        private void DoSerialize(T objectGraph, MemoryStream stream)
        {
            this.formatter.Serialize(stream, objectGraph);
        }
    }
}
