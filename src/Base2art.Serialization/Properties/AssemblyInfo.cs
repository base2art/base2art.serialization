using System.Reflection;

[assembly: AssemblyTitle("Base2art.Serialiation")]
[assembly: AssemblyDescription("Base2art simple component library")]
[assembly: AssemblyConfiguration("")]

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "Base2art.Serialization", Justification = "SjY")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "Base2art.Serialization.ItemSerialization", Justification = "SjY")]    