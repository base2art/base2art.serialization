﻿namespace Base2art.Serialization
{
    using System;
    using Base2art.Serialization.ItemSerialization;
    
    public static class Cloner
    {
        public static T CloneObject<T>(this T item)
        {
            return item.CloneObject(new NewtonsoftSerializer());
        }
        
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "SjY")]
        public static T CloneObject<T>(this T item, ISerializer serializer)
        {
            if (item == null)
            {
                return item;
            }
            
            if (item.GetType().IsValueType)
            {
                return item;
            }
            
            var clonable = item as ICloneable;
            
            if (clonable != null)
            {
                return (T)clonable.Clone();
            }
            
            if (serializer != null)
            {
                try
                {
                    var s = serializer.Serialize(item);
                    var rez = serializer.Deserialize<T>(s);
                    return rez;
                }
                catch (Exception)
                {
                }
            }
            
            var binSerializer = new BinaryItemSerializer<T>();
            return binSerializer.Clone(item);
        }
    }
}
