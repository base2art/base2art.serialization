﻿namespace Base2art.Serialization
{
    using System;
    using System.IO;
    
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Newtonsoft", Justification = "SjY")]
    public sealed class NewtonsoftSerializer : ISerializer
    {
        public string Serialize<T>(T item)
        {
            return BaseSerializer.Serialize<T>(item);
        }
        
        public T Deserialize<T>(string text)
        {
            return BaseSerializer.Deserialize<T>(text);
        }
    }
}

/*
 
        private static readonly ExternalProxyObjectWrapper<INewtonsoftSerializerProxy> Serializer = ExternalProxyObject.CreateWrapper<INewtonsoftSerializerProxy>(
            assembly =>
            {
                Func<string, Type> createType = assembly.GetType;
                Func<string, object> create = typeName => Activator.CreateInstance(createType(typeName));
                
                dynamic instance = create("Newtonsoft.Json.JsonSerializer");
                
                instance.Formatting = (dynamic)Enum.Parse(createType("Newtonsoft.Json.Formatting"), "Indented");
                instance.Converters.Add((dynamic)create("Newtonsoft.Json.Converters.StringEnumConverter"));
                instance.Converters.Add((dynamic)create("Newtonsoft.Json.Converters.VersionConverter"));
                return instance;
            },
            "Newtonsoft.Json.dll");
        
        private interface INewtonsoftSerializerProxy
        {
            object Deserialize(TextReader reader, Type objectType);
            
            void Serialize(TextWriter textWriter, object value);
        }

        public string Serialize<T>(T item)
        {
            using (var ms = new MemoryStream())
            {
                using (var writer = new StreamWriter(ms))
                {
                    Serializer.Object.Serialize(writer, item);
                    writer.Flush();
                    
                    ms.Seek(0L, SeekOrigin.Begin);
                    
                    using (var reader = new StreamReader(ms))
                    {
                        return reader.ReadToEnd();
                    }
                }
            }
        }
        
        public T Deserialize<T>(string text)
        {
            using (var ms = new MemoryStream())
            {
                using (var writer = new StreamWriter(ms))
                {
                    writer.Write(text);
                    writer.Flush();
                    
                    ms.Seek(0L, SeekOrigin.Begin);
                    
                    using (var reader = new StreamReader(ms))
                    {
                        return (T)Serializer.Object.Deserialize(reader, typeof(T));
                    }
                }
            }
        }
 */