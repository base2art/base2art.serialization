﻿namespace Base2art.Serialization
{
    using System;
    using System.IO;
    using System.Reflection;

    internal static class BaseSerializer
    {
        private static readonly Lazy<dynamic> BackingSerializer = new Lazy<dynamic>(
            () =>
            {
                var bytes = GetBytes();
                Assembly assembly = Assembly.Load(bytes);
                Func<string, Type> createType = assembly.GetType;
                Func<string, object> create = typeName => Activator.CreateInstance(createType(typeName));
                dynamic instance = create("Newtonsoft.Json.JsonSerializer");
                instance.Formatting = (dynamic)Enum.Parse(createType("Newtonsoft.Json.Formatting"), "Indented");
                instance.Converters.Add((dynamic)create("Newtonsoft.Json.Converters.StringEnumConverter"));
                instance.Converters.Add((dynamic)create("Newtonsoft.Json.Converters.VersionConverter"));
                return instance;
            });

        public static string Serialize<T>(T item)
        {
            return Serialize(BackingSerializer.Value, item);
        }

        public static T Deserialize<T>(string value)
        {
            return (T)Deserialize(BackingSerializer.Value, value, typeof(T));
        }

        private static string Serialize(dynamic instance, object item)
        {
            using (var ms = new MemoryStream())
            {
                using (var writer = new StreamWriter(ms))
                {
                    instance.Serialize(writer, item);
                    writer.Flush();
                    ms.Seek(0L, SeekOrigin.Begin);
                    using (var reader = new StreamReader(ms))
                    {
                        return reader.ReadToEnd();
                    }
                }
            }
        }

        private static object Deserialize(dynamic instance, string value, Type type)
        {
            using (var reader = new StringReader(value))
            {
                return instance.Deserialize(reader, type);
            }
        }

        private static byte[] GetBytes()
        {
            var name = "Base2art.Serialization.ReferencedBinaries.Newtonsoft.Json.dll";
            Assembly currentAsm = Assembly.GetExecutingAssembly();
            using (var stream = currentAsm.GetManifestResourceStream(name))
            {
                var @byte = stream.ReadFully();
                return @byte;
            }
        }

        private static byte[] ReadFully(this Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0) 
                {
                    ms.Write(buffer, 0, read);
                }
                
                ms.Flush();
                return ms.ToArray();
            }
        }
    }
}
