﻿namespace Base2art.Serialization
{
    public interface ISerializer
    {
        string Serialize<T>(T item);
        
        T Deserialize<T>(string text);
    }
}
