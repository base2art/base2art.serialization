﻿namespace Base2art.Serialization
{
    using System;
    using Base2art.Serialization.ItemSerialization;
    using FluentAssertions;
    using Moq;
    using NUnit.Framework;
    using Base2art.Fixtures;
    
    [TestFixture]
    public class ClonerFeature
    {
        [SetUp]
        public void BeforeEach()
        {
            //            Serializer.DefaultSerializer = null;
        }
        
        [TearDown]
        public void AfterEach()
        {
            //            Serializer.DefaultSerializer = null;
        }
        
        [Test]
        public void ShouldClone_Null()
        {
            Cloner.CloneObject<string>(null).Should().BeNull();
        }
        
        
        [Test]
        public void ShouldThrowExceptionSerializer_NullArgs()
        {
            var serializer = new BinaryItemSerializer<Person>();
            new Action(() => serializer.Serialize(null)).ShouldThrow<ArgumentNullException>();
            new Action(() => serializer.Deserialize(null)).ShouldThrow<ArgumentNullException>();
            new Action(() => serializer.Deserialize(new byte[0])).ShouldThrow<ArgumentException>();
        }
        
        
        [Test]
        public void ShouldClone_Types()
        {
            "SjY".CloneObject().Should().Be("SjY");
            1.CloneObject().Should().Be(1);
            1L.CloneObject().Should().Be(1L);
        }
        
        [Test]
        public void ShouldClone_WithSerialization()
        {
            var person = new Person(){Name = "SjY"};
            
            var clonedPerson = person.CloneObject();
            
            clonedPerson.Name.Should().Be("SjY");
        }
        
        [Test]
        public void ShouldSerialize_WithSerialization()
        {
            var person = new Person(){Name = "SjY"};
            
            var result = new NewtonsoftSerializer().Serialize(person);
            
            result.Should().Be("{\r\n  \"Name\": \"SjY\",\r\n  \"Aliases\": null,\r\n  \"HighestGradeLevel\": \"MiddleSchool\",\r\n  \"Birthday\": \"0001-01-01T00:00:00\"\r\n}");
        }
        
        [Test]
        public void Should_Serialize_Deserialize_ManualCall()
        {
            var person = new SerializablePerson { Name = "SjY" };
            
            var serializer = new BinaryItemSerializer<SerializablePerson>();
            var result = serializer.Serialize(person);
            
            var person1 = serializer.Deserialize(result);
            
            var ser = new NewtonsoftSerializer();
            
            ser.Serialize(person)
                .Should().Be(ser.Serialize(person1));
//            result.Should().Be("{\r\n  \"Name\": \"SjY\",\r\n  \"Aliases\": null,\r\n  \"HighestGradeLevel\": \"MiddleSchool\",\r\n  \"Birthday\": \"0001-01-01T00:00:00\"\r\n}");
        }
        
        [Test]
        public void ShouldSerialize_WithSerialization_UsingDefaultCache()
        {
            Action call = () => {
                var person = new Person(){Name = "SjY"};
                
                var result = new NewtonsoftSerializer().Serialize(person);
                
                result.Should().Be("{\r\n  \"Name\": \"SjY\",\r\n  \"Aliases\": null,\r\n  \"HighestGradeLevel\": \"MiddleSchool\",\r\n  \"Birthday\": \"0001-01-01T00:00:00\"\r\n}");
            };
            
            call();
            call();
        }
        
        [Test]
        public void ShouldClone_Implements_IClonable()
        {
            var person = new ClonablePerson(){Name = "SjY"};
            
            var clonedPerson = person.CloneObject();
            
            clonedPerson.Name.Should().Be("SjY");
        }
        
        [Test]
        public void ShouldClone_ReusesInstance()
        {
            {
                var person = new Person(){Name = "SjY"};
                var clonedPerson = person.CloneObject();
                clonedPerson.Name.Should().Be("SjY");
            }
            
            {
                var person = new Person(){Name = "SjY"};
                var clonedPerson = person.CloneObject();
                clonedPerson.Name.Should().Be("SjY");
            }
        }
        
        [Test]
        public void ShouldClone_Implements_NotIClonable_NullSerializer()
        {
            var person = new Person { Name = "SjY" };
            Action mut = () => person.CloneObject(null);
            mut.ShouldThrow<System.Runtime.Serialization.SerializationException>();
        }
        
        [Test]
        public void ShouldClone_Implements_NotIClonable_SerializerThrowingException()
        {
            Mock<ISerializer> serializer = new Mock<ISerializer>(MockBehavior.Strict);
            var person = new Person { Name = "SjY" };
            Action mut = () => person.CloneObject(serializer.Object);
            mut.ShouldThrow<System.Runtime.Serialization.SerializationException>();
        }
        
        [Test]
        public void ShouldClone_Implements_NotIClonable_NullSerializer_Serialiable()
        {
            var person = new SerializablePerson { Name = "SjY" };
            //            Action mut = () => person.CloneObject(null);
            //            mut.ShouldThrow<System.Runtime.Serialization.SerializationException>();
            person.CloneObject(null).Should().NotBeNull();
        }
        
        [Test]
        public void ShouldClone_Implements_IClonable_DllNotFound()
        {
            var person = new ClonablePerson { Name = "SjY" };
            
            Func<ClonablePerson> mut = () => person.CloneObject(null);
            var item = mut();
            item.Should().NotBeNull();
        }
        
        [Test]
        public void ShouldClone_Implements_IClonable_BinarySerializer()
        {
            var person = new SerializablePerson { Name = "SjY" };
            
            var clonedPerson = person.CloneObject();
            
            clonedPerson.Name.Should().Be("SjY");
        }
        
        public ClonerFeature()
        {
        }
    }
}
